`timescale 1ns / 1ps
`include"IOBUF.v"
`include"glbl.v"
`include"issiflashwrapper.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/23/2020 06:00:00 PM
// Design Name: 
// Module Name: TB_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TB_top();


  reg  CLK_tck_clk;
  reg  RST_N_trst;
  reg  CLK;
  reg  RST_N;

  // action method ifc_uart0_sin
  reg  uart0_SIN;

  // value method ifc_uart0_sout
  wire uart0_SOUT;

  // action method boot_config
  reg  [1 : 0] boot_config = 1;

  // action method ifc_gpio_gpio_in_val
  reg  [21 : 0] gpio_in_val;

  // value method ifc_gpio_gpio_in_en
  wire [21 : 0] gpio_in_en;

  // value method ifc_gpio_gpio_out_val
  wire [21 : 0] gpio_out_val;

  // value method ifc_gpio_gpio_out_en
  wire [21 : 0] gpio_out_en;

  // value method ifc_gpio_gpio_pullup_en
  wire [21 : 0] gpio_pullup_en;

  // value method ifc_gpio_gpio_drive_0
  wire [21 : 0] gpio_drive_0;

  // value method ifc_gpio_gpio_drive_1
  wire [21 : 0] gpio_drive_1;

  // value method ifc_gpio_gpio_drive_2
  wire [21 : 0] gpio_drive_2;

  // value method ddr_slave_m_awvalid
  wire ddr_AWVALID;

  // value method ddr_slave_m_awid
  wire ddr_AWID;

  // value method ddr_slave_m_awaddr
  wire [31 : 0] ddr_AWADDR;

  // value method ddr_slave_m_awlen
  wire [7 : 0] ddr_AWLEN;

  // value method ddr_slave_m_awsize
  wire [2 : 0] ddr_AWSIZE;

  // value method ddr_slave_m_awburst
  wire [1 : 0] ddr_AWBURST;

  // value method ddr_slave_m_awlock
  wire ddr_AWLOCK;

  // value method ddr_slave_m_awcache
  wire [3 : 0] ddr_AWCACHE;

  // value method ddr_slave_m_awprot
  wire [2 : 0] ddr_AWPROT;

  // value method ddr_slave_m_awqos
  wire [3 : 0] ddr_AWQOS;

  // value method ddr_slave_m_awregion
  wire [3 : 0] ddr_AWREGION;

  // value method ddr_slave_m_awuser

  // action method ddr_slave_m_awready
  reg  ddr_AWREADY;

  // value method ddr_slave_m_wvalid
  wire ddr_WVALID;

  // value method ddr_slave_m_wdata
  wire [63 : 0] ddr_WDATA;

  // value method ddr_slave_m_wstrb
  wire [7 : 0] ddr_WSTRB;

  // value method ddr_slave_m_wlast
  wire ddr_WLAST;

  // value method ddr_slave_m_wuser

  // action method ddr_slave_m_wready
  reg  ddr_WREADY;

  // action method ddr_slave_m_bvalid
  reg  ddr_BVALID;
  reg  ddr_BID;
  reg  [1 : 0] ddr_BRESP;

  // value method ddr_slave_m_bready
  wire ddr_BREADY;

  // value method ddr_slave_m_arvalid
  wire ddr_ARVALID;

  // value method ddr_slave_m_arid
  wire ddr_ARID;

  // value method ddr_slave_m_araddr
  wire [31 : 0] ddr_ARADDR;

  // value method ddr_slave_m_arlen
  wire [7 : 0] ddr_ARLEN;

  // value method ddr_slave_m_arsize
  wire [2 : 0] ddr_ARSIZE;

  // value method ddr_slave_m_arburst
  wire [1 : 0] ddr_ARBURST;

  // value method ddr_slave_m_arlock
  wire ddr_ARLOCK;

  // value method ddr_slave_m_arcache
  wire [3 : 0] ddr_ARCACHE;

  // value method ddr_slave_m_arprot
  wire [2 : 0] ddr_ARPROT;

  // value method ddr_slave_m_arqos
  wire [3 : 0] ddr_ARQOS;

  // value method ddr_slave_m_arregion
  wire [3 : 0] ddr_ARREGION;

  // value method ddr_slave_m_aruser

  // action method ddr_slave_m_arready
  reg  ddr_ARREADY;

  // action method ddr_slave_m_rvalid
  reg  ddr_RVALID;
  reg  ddr_RID;
  reg  [63 : 0] ddr_RDATA;
  reg  [1 : 0] ddr_RRESP;
  reg  ddr_RLAST;

  // value method ddr_slave_m_rready
  wire ddr_RREADY;

  // value method open_slave_m_awvalid
  wire open_AWVALID;

  // value method open_slave_m_awid
  wire open_AWID;

  // value method open_slave_m_awaddr
  wire [31 : 0] open_AWADDR;

  // value method open_slave_m_awlen
  wire [7 : 0] open_AWLEN;

  // value method open_slave_m_awsize
  wire [2 : 0] open_AWSIZE;

  // value method open_slave_m_awburst
  wire [1 : 0] open_AWBURST;

  // value method open_slave_m_awlock
  wire open_AWLOCK;

  // value method open_slave_m_awcache
  wire [3 : 0] open_AWCACHE;

  // value method open_slave_m_awprot
  wire [2 : 0] open_AWPROT;

  // value method open_slave_m_awqos
  wire [3 : 0] open_AWQOS;

  // value method open_slave_m_awregion
  wire [3 : 0] open_AWREGION;

  // value method open_slave_m_awuser

  // action method open_slave_m_awready
  reg  open_AWREADY;

  // value method open_slave_m_wvalid
  wire open_WVALID;

  // value method open_slave_m_wdata
  wire [63 : 0] open_WDATA;

  // value method open_slave_m_wstrb
  wire [7 : 0] open_WSTRB;

  // value method open_slave_m_wlast
  wire open_WLAST;

  // value method open_slave_m_wuser

  // action method open_slave_m_wready
  reg  open_WREADY;

  // action method open_slave_m_bvalid
  reg  open_BVALID;
  reg  open_BID;
  reg  [1 : 0] open_BRESP;

  // value method open_slave_m_bready
  wire open_BREADY;

  // value method open_slave_m_arvalid
  wire open_ARVALID;

  // value method open_slave_m_arid
  wire open_ARID;

  // value method open_slave_m_araddr
  wire [31 : 0] open_ARADDR;

  // value method open_slave_m_arlen
  wire [7 : 0] open_ARLEN;

  // value method open_slave_m_arsize
  wire [2 : 0] open_ARSIZE;

  // value method open_slave_m_arburst
  wire [1 : 0] open_ARBURST;

  // value method open_slave_m_arlock
  wire open_ARLOCK;

  // value method open_slave_m_arcache
  wire [3 : 0] open_ARCACHE;

  // value method open_slave_m_arprot
  wire [2 : 0] open_ARPROT;

  // value method open_slave_m_arqos
  wire [3 : 0] open_ARQOS;

  // value method open_slave_m_arregion
  wire [3 : 0] open_ARREGION;

  // value method open_slave_m_aruser

  // action method open_slave_m_arready
  reg  open_ARREADY;

  // action method open_slave_m_rvalid
  reg  open_RVALID;
  reg  open_RID;
  reg  [63 : 0] open_RDATA;
  reg  [1 : 0] open_RRESP;
  reg  open_RLAST;

  // value method open_slave_m_rready
  wire open_RREADY;

  // action method wire_tms
  reg  wire_tms_tms_in;

  // action method wire_tdi
  reg  wire_tdi_tdi_in;

  // value method wire_tdo
  wire wire_tdo;

  // actionvalue method io_dump_get
  reg  EN_io_dump_get;
  wire [167 : 0] io_dump_get;
  wire RDY_io_dump_get;

  // value method qspi_io_clk_o
  wire qspi_io_clk_o;

  // value method qspi_io_io_o
  wire [3 : 0] qspi_io_io_o;

  // value method qspi_io_io_enable
  wire [3 : 0] qspi_io_io_enable;

  // action method qspi_io_io_i
  //reg  [3 : 0] qspi_io_io_i_io_i;
  wire  [3 : 0] qspi_io_io_i_io_i;

  // value method qspi_io_ncs_o
  wire qspi_io_ncs_o;

  // wire resets
  wire RST_N_soc_reset;


  initial begin
      CLK = 0;
	  RST_N = 1;
	  #50;
	  RST_N = 0;
	  #500;
	  RST_N = 1;
  
  end
  
  always #5 CLK = ~CLK;

  assign CLK_tck_clk = CLK;
  assign RST_N_trst = RST_N;


  
  glbl glbl_i ();
 
  wire io_0;
  wire io_1;
  wire io_2;
  wire io_3;
  
  
    
    IOBUF gpio_iobuf_inst1 (
        .O  (qspi_io_io_i_io_i[0]),
        .IO (io_0),
        .I  (qspi_io_io_o[0]),
        .T  (~qspi_io_io_enable[0]) //high to flash low is from flash
      );

 

      IOBUF gpio_iobuf_inst2 (
        .O  (qspi_io_io_i_io_i[1]),
        .IO (io_1),
        .I  (qspi_io_io_o[1]),
        .T  (~qspi_io_io_enable[1])
      );
      
      IOBUF gpio_iobuf_inst3 (
        .O  (qspi_io_io_i_io_i[2]),
        .IO (io_2),
        .I  (qspi_io_io_o[2]),
        .T  (~qspi_io_io_enable[2])
      );      
      
      IOBUF gpio_iobuf_inst4 (
        .O  (qspi_io_io_i_io_i[3]),
        .IO (io_3),
        .I  (qspi_io_io_o[3]),
        .T  (~qspi_io_io_enable[3])
      );
      
  
           
//----------------------------------------------------
// Instantiation of ISSI_Flash_Memory module               
//----------------------------------------------------


issiflashwrapper flash_inst(
        .sclk   (qspi_io_clk_o),
        .cs     (qspi_io_ncs_o),
        .si     (io_0),
        .so     (io_1),
        .wp     (io_2),
        .sio3   (io_3) 
);   
  
  












    mkDebugSoc mkDebugSoc_i(.CLK_tck_clk(CLK_tck_clk),		                    
		                    .RST_N_trst(RST_N_trst),
		                    
		                    .CLK(CLK),
		                    .RST_N(RST_N),
		                    
		                    .uart0_SIN(uart0_SIN),
		                    .uart0_SOUT(uart0_SOUT),
		                    
		                    .boot_config(boot_config),
		                    
		                    .gpio_in_val(gpio_in_val),
		                    .gpio_in_en(gpio_in_en),
		                    .gpio_out_val(gpio_out_val),
		                    .gpio_out_en(gpio_out_en),
		                    .gpio_pullup_en(gpio_pullup_en),
		                    .gpio_drive_0(gpio_drive_0),
		                    .gpio_drive_1(gpio_drive_1),
		                    .gpio_drive_2(gpio_drive_2),
		                    
		                    .ddr_AWVALID(ddr_AWVALID),
		                    .ddr_AWID(ddr_AWID),
		                    .ddr_AWADDR(ddr_AWADDR),
		                    .ddr_AWLEN(ddr_AWLEN),
		                    .ddr_AWSIZE(ddr_AWSIZE),
		                    .ddr_AWBURST(ddr_AWBURST),
		                    .ddr_AWLOCK(ddr_AWLOCK),
		                    .ddr_AWCACHE(ddr_AWCACHE),
		                    .ddr_AWPROT(ddr_AWPROT),
		                    .ddr_AWQOS(ddr_AWQOS),
		                    .ddr_AWREGION(ddr_AWREGION),
		                    .ddr_AWREADY(ddr_AWREADY),
		                    .ddr_WVALID(ddr_WVALID),
		                    .ddr_WDATA(ddr_WDATA),
		                    .ddr_WSTRB(ddr_WSTRB),
		                    .ddr_WLAST(ddr_WLAST),
		                    .ddr_WREADY(ddr_WREADY),
		                    .ddr_BVALID(ddr_BVALID),
		                    .ddr_BID(ddr_BID),
		                    .ddr_BRESP(ddr_BRESP),
		                    .ddr_BREADY(ddr_BREADY),
		                    .ddr_ARVALID(ddr_ARVALID),
		                    .ddr_ARID(ddr_ARID),
		                    .ddr_ARADDR(ddr_ARADDR),
		                    .ddr_ARLEN(ddr_ARLEN),
		                    .ddr_ARSIZE(ddr_ARSIZE),
		                    .ddr_ARBURST(ddr_ARBURST),
		                    .ddr_ARLOCK(ddr_ARLOCK),
		                    .ddr_ARCACHE(ddr_ARCACHE),
		                    .ddr_ARPROT(ddr_ARPROT),
		                    .ddr_ARQOS(ddr_ARQOS),
		                    .ddr_ARREGION(ddr_ARREGION),
		                    .ddr_ARREADY(ddr_ARREADY),
		                    .ddr_RVALID(ddr_RVALID),
		                    .ddr_RID(ddr_RID),
		                    .ddr_RDATA(ddr_RDATA),
		                    .ddr_RRESP(ddr_RRESP),
		                    .ddr_RLAST(ddr_RLAST),
		                    .ddr_RREADY(ddr_RREADY),

		                    .open_AWVALID(open_AWVALID),
		                    .open_AWID(open_AWID),
		                    .open_AWADDR(open_AWADDR),
		                    .open_AWLEN(open_AWLEN),
		                    .open_AWSIZE(open_AWSIZE),
		                    .open_AWBURST(open_AWBURST),
		                    .open_AWLOCK(open_AWLOCK),
		                    .open_AWCACHE(open_AWCACHE),
		                    .open_AWPROT(open_AWPROT),
		                    .open_AWQOS(open_AWQOS),
		                    .open_AWREGION(open_AWREGION),
		                    .open_AWREADY(open_AWREADY),
		                    .open_WVALID(open_WVALID),
		                    .open_WDATA(open_WDATA),
		                    .open_WSTRB(open_WSTRB),
		                    .open_WLAST(open_WLAST),
		                    .open_WREADY(open_WREADY),
		                    .open_BVALID(open_BVALID),
		                    .open_BID(open_BID),
		                    .open_BRESP(open_BRESP),
		                    .open_BREADY(open_BREADY),
		                    .open_ARVALID(open_ARVALID),
		                    .open_ARID(open_ARID),
		                    .open_ARADDR(open_ARADDR),
		                    .open_ARLEN(open_ARLEN),
		                    .open_ARSIZE(open_ARSIZE),
		                    .open_ARBURST(open_ARBURST),
		                    .open_ARLOCK(open_ARLOCK),
		                    .open_ARCACHE(open_ARCACHE),
		                    .open_ARPROT(open_ARPROT),
		                    .open_ARQOS(open_ARQOS),
		                    .open_ARREGION(open_ARREGION),
		                    .open_ARREADY(open_ARREADY),
		                    .open_RVALID(open_RVALID),
		                    .open_RID(open_RID),
		                    .open_RDATA(open_RDATA),
		                    .open_RRESP(open_RRESP),
		                    .open_RLAST(open_RLAST),
		                    .open_RREADY(open_RREADY),
		                    
		                    .wire_tms_tms_in(wire_tms_tms_in),
		                    .wire_tdi_tdi_in(wire_tdi_tdi_in),
		                    .wire_tdo(wire_tdo),
		                    
		                    .EN_io_dump_get(EN_io_dump_get),
		                    .io_dump_get(io_dump_get),
		                    .RDY_io_dump_get(RDY_io_dump_get),
		                    
		                    .qspi_io_clk_o(qspi_io_clk_o),
		                    .qspi_io_io_o(qspi_io_io_o),
		                    .qspi_io_io_enable(qspi_io_io_enable),
		                    .qspi_io_io_i_io_i(qspi_io_io_i_io_i),
		                    .qspi_io_ncs_o(qspi_io_ncs_o),
		                    .RST_N_soc_reset(RST_N_soc_reset)
    );
endmodule
