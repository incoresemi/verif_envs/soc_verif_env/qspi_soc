//************************************************************************************************
// C File Name      : common_functions.c
// Purpose          : This file consists of functions which perform basic common functionalities
//************************************************************************************************


//-------------------------------------------------------------------------------------------
//To perform write operation to a register
//Address of the register and the data to be written are to be passed as arguments
//-------------------------------------------------------------------------------------------

void write_reg(unsigned int reg_addr,unsigned int data) 
        {
            unsigned int *addr=reg_addr;
            *(addr) = data;
        }

        
//-------------------------------------------------------------------------------------------
//To perform read operation from a register
//Address of the register is to be passed as arguments
//It prints the data present in the register
//-------------------------------------------------------------------------------------------
void read_reg(unsigned int reg_addr)
        {
            unsigned int *r_addr=(unsigned int *)reg_addr;
            int rd_data;
            rd_data = *(r_addr);
            printf("%x",rd_data);
        }
        

//-------------------------------------------------------------------------------------------
//To insert a delay
//Required time need to be passed as arguments
//for time = 1000, delay of 40us is generated
//It prints the alphabet D followed by the count value based on the time specified
//-------------------------------------------------------------------------------------------
void delay(unsigned long time) 
        {
            unsigned long i,j,k = 0;
                for(i=0;i<time;i++)
                {
                for(j=0;j<time;j++)
                {
                for(j=0;j<time;j++)
                {
                k++;
                }
                }
                }
            printf("D%d",k);
        }


//-------------------------------------------------------------------------------------------
//To poll for TCF flag untill it is set
//SR is to be passed as arguments during the function call
//SR is periodically read at every 20us 
//-------------------------------------------------------------------------------------------
void wait_for_tcf_flag(unsigned int reg_addr)
{
    unsigned int *r_addr=(unsigned int *)reg_addr;
    int rd_data;
        rd_data = *(r_addr);
    
        while(!(rd_data & 0x2)){
        delay(500);  
        rd_data = *(r_addr);
        }
}

