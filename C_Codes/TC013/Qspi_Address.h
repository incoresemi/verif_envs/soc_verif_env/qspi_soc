//******************************************************************************************
// File Name        : Qspi_Address.h
// Purpose          : This file consists of base addresses of all the available registers
//                    of QSPI controller
//******************************************************************************************

#ifndef Qspi_Address_H
#define Qspi_Address_H

// Registers base address of QSPI Module
#define CR      0x00040000       // Control Register
#define DCR     0x00040004       // Device Configuration Register
#define SR      0x00040008       // Status Register
#define FCR     0x0004000c       // Flag Clear Register
#define DLR     0x00040010       // Data Length Register
#define CCR     0x00040014       // Communication Configuration Register
#define AR      0x00040018       // Address Register
#define ABR     0x0004001c       // Alternate Byte Register
#define DR      0x00040020       // Data Register
#define PSMKR   0x00040024       // Polling Status Mask Register
#define PSMAR   0x00040028       // Polling Status Mark Register
#define PIR     0x0004002c       // Polling Interval Register
#define LPTR    0x00040030       // Low Power Time out Register

#endif