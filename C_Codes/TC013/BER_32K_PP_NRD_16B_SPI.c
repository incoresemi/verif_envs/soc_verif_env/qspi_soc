//*************************************************************************************************
// File Name        : BER_32K_PP_NRD_16B_SPI.c
// Purpose          : This file illustrates the test case to implement the following 
//                    32K Block Erase, Page Program and Normal Read of 16 Bytes data 
//                    for 3 byte Address in SPI mode
//*************************************************************************************************

// Header files and Function files
#include <stdio.h>
#include "Qspi_Address.h"
#include "common_functions.c"
#include "Qspi_functions.c"


void main()
        {

          //--------------------------------------------------------------------------------------
          //Prints the amity of two companies for initial testing purpose
          //--------------------------------------------------------------------------------------
            printf("TCS_INCORE");

          //--------------------------------------------------------------------------------------
          //Initialization of sector address which will be passed as arguments to some functions
          //--------------------------------------------------------------------------------------
            int write_address = 0x00010000;

          //--------------------------------------------------------------------------------------
          //Function call to initialize the QSPI controller module
          //--------------------------------------------------------------------------------------
            qspi_init(0x031f0f01,0x001b0001);

          //--------------------------------------------------------------------------------------
          //                        *********************
          //Function call to Erase  ** sector of flash **
          //                        *********************
          //--------------------------------------------------------------------------------------
          // S_B_C_Erase(0x00002520,write_address); //Sector Erase either D7 or 20


          //--------------------------------------------------------------------------------------
          //                        ********************
          //Function call to Erase  ** Block of flash **
          //                        ********************
          //--------------------------------------------------------------------------------------
            S_B_C_Erase(0x00002552,write_address);//32k
            //S_B_C_Erase(0x000025d8,write_address);//64k


         
          //--------------------------------------------------------------------------------------
          //Function call to perform Page program to flash
          //--------------------------------------------------------------------------------------
            PP_SPI(0x01002502,write_address);

          //--------------------------------------------------------------------------------------
          //Function call to perform read operation from flash
          //--------------------------------------------------------------------------------------

            
            //Normal Read(NRD) max 80 MHz
           read_data(0x0000000f,0x05002503,write_address); 

            //Fast Read(FRD) max 166 MHz in SPI with 8 dummy cycles and 104MHz in QPI
            //read_data(0x0000000f,0x0520250b,write_address);  

           
            //Fast Read both IO0,IO1(FRDIO) in SPI max  MHz with dummy cycles 4
           //read_data(0x0000000f,0x061029bb,write_address); 

            //Fast Read both IO0,IO1(FRDO) in SPI max 166 MHz with dummy cycles 8
          // read_data(0x0000000f,0x0620253b,write_address); 

        }
