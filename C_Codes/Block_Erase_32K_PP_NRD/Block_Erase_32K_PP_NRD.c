//*************************************************************************************************
// File Name        : ER_PP_RD_16_BYTES_SPI.c
// Purpose          : This file illustrates the test case to implement the following
//                    Erase, Page Program and Read of 16 Bytes data in SPI mode
// version          : 
//*************************************************************************************************

// Header files and Function files
#include <stdio.h>
#include "Qspi_Address.h"
#include "common_functions.c"
#include "Qspi_functions.c"


void main()
        {

          //--------------------------------------------------------------------------------------
          //Prints the amity of two companies for initial testing purpose
          //--------------------------------------------------------------------------------------
            printf("TCS_INCORE");

          //--------------------------------------------------------------------------------------
          //Initialization of sector address which will be passed as arguments to some functions
          //--------------------------------------------------------------------------------------
            int write_address = 0x00010000;

          //--------------------------------------------------------------------------------------
          //Function call to initialize the QSPI controller module
          //CR: PRESCALER=0x03, TOIE=1, SMIE=1, FTIE=1, TCIE=1, TEIE=1, FTHRES=0xf, EN=1
          //DCR: FSIZE=0x1b, CSHT=0, CKMODE=1
          //--------------------------------------------------------------------------------------
            qspi_init(0x031f0f01,0x001b0001);

          //--------------------------------------------------------------------------------------
          //                        *********************
          //Function call to Erase  ** sector of flash **
          //                        *********************
          //CCR: FMODE=0, DMODE=0, ADSIZE=11, ADMODE=01, IMODE=01, INSTRUCTION=0x21
          //--------------------------------------------------------------------------------------
            //S_B_C_Erase(0x00003521,write_address);


          //--------------------------------------------------------------------------------------
          //                        ********************
          //Function call to Erase  ** Block of flash **
          //                        ********************
          //CCR: FMODE=, DMODE=, ADSIZE=, ADMODE=, IMODE=, INSTRUCTION=
          //--------------------------------------------------------------------------------------
            S_B_C_Erase(0x0000355c,write_address)//32k
            //S_B_C_Erase(0x000035dc,write_address)//64k


          //--------------------------------------------------------------------------------------
          //                        ********************
          //Function call to Erase  ** Chip of flash  **
          //                        ********************
          //CCR: FMODE=, DMODE=, ADSIZE=, ADMODE=, IMODE=, INSTRUCTION=
          //--------------------------------------------------------------------------------------
            //S_B_C_Erase(0x000035c7,write_address);
            //S_B_C_Erase(0x00003560,write_address);


          //--------------------------------------------------------------------------------------
          //Function call to perform Page program to flash
          //CCR: FMODE=0, DMODE=1, ADSIZE=11, ADMODE=01, IMODE=01, INSTRUCTION=0x12
          //--------------------------------------------------------------------------------------
            PP_SPI(0x01003512,write_address);

          //--------------------------------------------------------------------------------------
          //Function call to perform read operation from flash
          //--------------------------------------------------------------------------------------

            //CCR: FMODE=1, DMODE=1, DCYC=1000, ADSIZE=11, ADMODE=01, IMODE=01, INSTRUCTION=0x13
            //Normal Read(NRD) max 80 MHz
            read_data(0x0000000f,0x05203513,write_address); 

            //Fast Read(FRD) max 166 MHz in SPI with 8 dummy cycles and 104MHz in QPI
            //read_data(0x0000000f,0x0520350c,write_address);  

            //Fast Read both IO0,IO1(FRDIO) in SPI max 156 MHz with dummy cycles 8
            //read_data(0x0000000f,0x062035bc,write_address); 

            //Fast Read both IO0,IO1(FRDIO) in SPI max  MHz with dummy cycles 4
            //read_data(0x0000000f,0x061035bc,write_address); 

            //Fast Read both IO0,IO1(FRDO) in SPI max 166 MHz with dummy cycles 8
            //read_data(0x0000000f,0x0620353c,write_address); 

        }